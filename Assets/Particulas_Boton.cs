using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Particulas_Boton: MonoBehaviour
{
    public ParticleSystem Particle;
    // Start is called before the first frame update
    public void Odisea()
    {
        var noise = Particle.noise;
        noise.strength = 10;
    }
    public void ModifyRates()
    {
        var mainModule = Particle.main;
        mainModule.gravityModifier = 1f;
        mainModule.startColor = Color.green;
    }
    public void ModifyRates1()
    {
        var emissionModule = Particle.emission;
        emissionModule.rateOverTime = 800;
        emissionModule.rateOverDistance = 20;
    }

    public void IncreaseParticle(int increment)
    {
        var emissionModule = Particle.emission;
        emissionModule.rateOverTime = emissionModule.rateOverTime.constant + increment;
    }
}
